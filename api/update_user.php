<?php
/**
 * Created by PhpStorm.
 * User: alanmr
 * Date: 6/9/17
 * Time: 10:19 PM
 */
include 'config.php';
/*
 * validate token
 */
$token=(isset($_POST['token']))? $_POST['token'] : FALSE;

if($token){
    /*
     * check token
     */
    $tokenInfo=$db->get($config['table']['token'],null,'*',[
        'token'=>$token
    ]);
    /*
     * if token valid
     */
    if($tokenInfo){
        $session_id=$tokenInfo['session_id'];
        $fullname=(isset($_POST['fullname']))? $_POST['fullname'] : FALSE;
        $oldpassword=(isset($_POST['oldpassword']))? $_POST['oldpassword'] : FALSE;
        $password=(isset($_POST['password']))? $_POST['password'] : FALSE;
        $address=(isset($_POST['address']))? $_POST['address'] : FALSE;
        $nohp=(isset($_POST['nohp']))? $_POST['nohp'] : FALSE;
        $email=(isset($_POST['email']))? $_POST['email'] : FALSE;
        /*
         * check basic info
         */
        if($fullname && $address && $nohp && $email){
            /*
             * if change password
             */
            if($password){
                $user=$db->get($config['table']['user'],null,'*',[
                    'AND'=>[
                        'password'=>md5($oldpassword),
                        'session_id'=>$session_id
                    ]
                ]);
                /*
                 * check old password
                 */
                if($user){
                    $data=array(
                        's_nama'=>$fullname,
                        's_telp'=>$nohp,
                        's_password'=>md5($password),
                        's_email'=>$email,
                        's_alamat'=>$address
                    );
                    $update=$db->update($config['table']['user'],$data,[
                        'session_id'=>$session_id
                    ]);
                    /*
                     * if update user info
                     */
                    if($update){
                        $response['status']=true;
                        $response['message']="data has been updated";
                    }else{
                        $response['status']=false;
                        $response['message']="server error";
                    }
                }else{
                    $response['status']=false;
                    $response['message']="incorrect old password";
                }
            }else{
                $data=array(
                    's_nama'=>$fullname,
                    's_telp'=>$nohp,
                    's_email'=>$email,
                    's_address'=>$address
                );
                $update=$db->update($config['table']['user'],$data,[
                    'session_id'=>$session_id
                ]);
                /*
                 * if update user info
                 */
                if($update){
                    $response['status']=true;
                    $response['message']="data has been updated";
                }else{
                    $response['status']=false;
                    $response['message']="server error";
                }
            }
        }else{
            $response['status']=false;
            $response['message']="fill all data , please !";
        }
    }else{
        $response['status']=false;
        $response['message']="not authorized";
    }
}else{
    $response['status']=false;
    $response['message']="token needed";
}
echo json_encode($response);