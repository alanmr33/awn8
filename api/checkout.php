<?php
/**
 * Created by PhpStorm.
 * User: alanmr
 * Date: 6/9/17
 * Time: 10:48 PM
 */
include 'config.php';
/*
 * validate token
 */
$postdata = json_decode(file_get_contents("php://input"));
$token=(isset($postdata->token))? $postdata->token : FALSE;

if($token){
    /*
    * check token
    */
    $tokenInfo=$db->get($config['table']['token'],'*',[
        'token'=>$token
    ]);
    /*
     * if token valid
     */
    if($tokenInfo){
        /*
         * get user data
         */
        $user=$db->get($config['table']['user'],'*',[
           'session_id'=>$tokenInfo['session_id']
        ]);
        /*
         * input pesanna
         */
        $bank=(isset($postdata->bank))? $postdata->bank:FALSE;
        $ref=(isset($postdata->ref))? $postdata->ref:FALSE;
        $note=(isset($postdata->note))? $postdata->note:FALSE;
        /*
         * check parameters
         */
        if($ref && $ref && $bank){

            $detail=$postdata->detail;
            $nomer=$db->query("select max(pemesanan.pemesanan_kode) as ps_kode, max(pemesanan.no_transaksi) as no_ts from ".$config['table']['pemesanan'])->fetchAll();
            $nomer=$nomer[0];
            foreach ($detail as $d){
                $nomer['ps_kode']++;
                $nomer['no_ts']++;
                $room=$d->room;
                $date=$d->dateData;
                $data_detail=array(
                   'pemesanan_kode'=>$nomer['ps_kode'],
                   'no_transaksi'=>$nomer['no_ts'],
                   'nama_tempat'=>$d->tujuan,
                   'penginapan_kode'=>$room->penginapan_kode,
                   'pemesanan_nama'=>$user['s_nama'],
                   'pemesanan_email'=>$user['s_email'],
                   'pemesanan_telp'=>$user['s_telp'],
                   'pemesanan_alamat'=>$user['s_alamat'],
                   'bank'=>$bank,
                   'tgl_masuk'=>date('Y-m-d',strtotime($date->date_start)),
                   'tgl_keluar'=>date('Y-m-d',strtotime($date->date_end)),
                   'pemesanan_waktu_pesan'=>date('Y-m-d H:i:s'),
                   'pemesanan_pengaturan_harga_1'=>'7-4',
                   'pemesanan_pengaturan_harga_2'=>'5-6',
                   'pemesanan_total'=>$d->grandTotal,
                   'pemesanan_status'=>'Baru',
                   'pemesanan_booked_by'=>$ref,
                   'akses'=>'user'
                );
                $insert_detail=$db->insert($config['table']['pemesanan'],$data_detail);
            }
            /*
             * to send mail here
             */
            $response['status']=true;
            $response['message']="reservation on process";
        }else{
            $response['status']=false;
            $response['message']="invalid param";
        }
    }else{
        $response['status']=false;
        $response['message']="not authorized";
    }
}else{
    $response['status']=false;
    $response['message']="token needed";
}
echo json_encode($response);