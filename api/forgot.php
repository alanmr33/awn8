<?php
/**
 * Created by PhpStorm.
 * User: alanmr
 * Date: 6/9/17
 * Time: 10:08 PM
 */
include 'config.php';
/*
 * check email input
 */
$postdata = json_decode(file_get_contents("php://input"));
$email=(isset($postdata->email))? $postdata->email : FALSE;

if($email){
    /*
     * check user registered
     */
    $user=$db->get($config['table']['user'],'*',[
       's_email'=>$email
    ]);
    /*
     * if registered
     */
    if($user){
        /*
         * set reset code for user
         */
        $resetcode=md5(date('Y-m-d H:i:s'));
        $data=array(
            'reset'=>$resetcode
        );
        $forgot=$db->update($config['table']['user'],$data,[
            'session_id'=>$user['session_id']
        ]);
        if($forgot){
            /*
             * send forgot mail
             */
            $response['status']=true;
            $response['message']="an email for reset password has been sent.";
        }else{
            $response['status']=false;
            $response['message']="server errror";
        }
    }else{
        $response['status']=false;
        $response['message']="user not found, check your email correctly.";
    }
}else{
    $response['status']=false;
    $response['email']=$email;
    $response['message']="please, enter your email!";
}
echo json_encode($response);
