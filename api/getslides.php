<?php
/**
 * Created by PhpStorm.
 * User: alanmr
 * Date: 6/9/17
 * Time: 10:48 PM
 */
include 'config.php';
/*
 * validate token
 */
$postdata = json_decode(file_get_contents("php://input"));
$token=(isset($postdata->token))? $postdata->token : FALSE;
$kode_tujuan=(isset($postdata->kode_tujuan))? $postdata->kode_tujuan : FALSE;

if($token){
    /*
    * check token
    */
    $tokenInfo=$db->get($config['table']['token'],'*',[
        'token'=>$token
    ]);
    /*
     * if token valid
     */
    if($tokenInfo){
        /*
         * get all info
         */
        $info=$db->select($config['table']['slide'],'*',[
            'kode_tujuan'=>$kode_tujuan
        ]);
        if($info){
            $response['status']=true;
            $response['data']=$info;
        }else{
            $response['status']=true;
            $response['data']=array();
        }
    }else{
        $response['status']=false;
        $response['message']="not authorized";
    }
}else{
    $response['status']=false;
    $response['message']="token needed";
}
echo json_encode($response);