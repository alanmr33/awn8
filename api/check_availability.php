<?php
/**
 * Created by PhpStorm.
 * User: alanmr
 * Date: 6/9/17
 * Time: 10:48 PM
 */
include 'config.php';
/*
 * validate token
 */
$postdata = json_decode(file_get_contents("php://input"));
$token=(isset($postdata->token))? $postdata->token : FALSE;

if($token){
    /*
    * check token
    */
    $tokenInfo=$db->get($config['table']['token'],'*',[
        'token'=>$token
    ]);
    /*
     * if token valid
     */
    if($tokenInfo){
        /*
         * validate input
         */
        $kode_tujuan=(isset($postdata->kode_tujuan))? $postdata->kode_tujuan : FALSE;
        $date_start=(isset($postdata->date_start))? date('Y-m-d',strtotime($postdata->date_start)) : FALSE;
        $date_end=(isset($postdata->date_end))? date('Y-m-d',strtotime($postdata->date_end)) : FALSE;

        if($kode_tujuan && $date_start && $date_end){
            /*
             * get penginapan lis
             */
            $penginapan=$db->select($config['table']['penginapan'],'*',[
               'kode_tujuan'=>$kode_tujuan
            ]);
            if($penginapan){
                $available=array();
                /*
                 * check available rooms
                 */
                foreach ($penginapan as $p){
                    /*
                     * get current booked rooms if no booking add to available data
                     */
                    $pesanan=$db->select($config['table']['penginapan'],'*',[
                        'AND'=>[
                            'penginapan_kode'=>$p['penginapan_kode'],
                            'tgl_masuk[<>]'=>[$date_start,$date_end],
                            'tgl_keluar[<>]'=>[$date_start,$date_end]
                        ]
                    ]);
                    if(!$pesanan){
                        $available[]=$p;
                    }
                }
                $response['status']=true;
                $response['data']=$available;
                $response['message']="there's rooms";
            }else{
                $response['status']=false;
                $response['data']=array();
                $response['message']="no rooms";
            }
        }else{
            $response['status']=false;
            $response['data']=array();
            $response['message']="no data";
        }
    }else{
        $response['status']=false;
        $response['message']="not authorized";
    }
}else{
    $response['status']=false;
    $response['message']="token needed";
}
echo json_encode($response);