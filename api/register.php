<?php
/**
 * Created by PhpStorm.
 * User: alanmr
 * Date: 6/9/17
 * Time: 10:38 AM
 */
include 'config.php';
/*
 * validate input
 */
$postdata = json_decode(file_get_contents("php://input"));
$fullname=(isset($postdata->fullname))? $postdata->fullname : FALSE;
$password=(isset($postdata->password))? $postdata->password : FALSE;
$cpassword=(isset($postdata->cpassword))? $postdata->cpassword : FALSE;
$address=(isset($postdata->address))? $postdata->address : FALSE;
$nohp=(isset($postdata->nohp))? $postdata->nohp : FALSE;
$email=(isset($postdata->email))? $postdata->email : FALSE;

if($fullname && $password && $cpassword && $address && $nohp && $email){
    /*
     * check email/nohp if registered
     */
    $user=$db->get($config['table']['user'],'*',[
        'OR'=>[
            's_telp'=>$nohp,
            's_email'=>$email
        ]
    ]);
    /*
     * registered
     */
    if($user){
        $response['status']=false;
        $response['message']="user already registered";
    }else{
       $data=array(
            's_nama'=>$fullname,
            's_password'=>md5($password),
            's_telp'=>$nohp,
            's_email'=>$email,
            's_alamat'=>$address,
            'status'=>'aktif'
       );
       $register=$db->insert($config['table']['user'],$data);
       /*
        * if register success
        */
       if($register){
            /*
             * email here
             */
           /*
            * create token app access
            */
           $token=md5(date('Y-m-d H:i:s').$email);
           $token_data=array(
               'token'=>$token,
               'session_id'=>$db->id()
           );
           /*
            * insert to db
            */
           $db->insert($config['table']['token'],$token_data);
           $response['status']=true;
           $response['message']="registration success";
           $response['token']=$token;
       }else{
           $response['status']=false;
           $response['message']="server error";
       }
    }
}else{
    $response['status']=false;
    $response['message']="fill all data , please !";
}
echo json_encode($response);
