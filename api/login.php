<?php
/**
 * Created by PhpStorm.
 * User: alanmr
 * Date: 6/9/17
 * Time: 1:54 PM
 */
include 'config.php';
/*
 * switch type login sosmed or normal
 */
$postdata = json_decode(file_get_contents("php://input"));
$type_login=(isset($postdata->type))? $postdata->type : FALSE;
switch ($type_login){
    /*
     * normal login default
     */
    default :
        $email=(isset($postdata->email))? $postdata->email : FALSE;
        $password=(isset($postdata->password))? md5($postdata->password) : FALSE;
        /*
         * auth
         */
        $user=$db->get($config['table']['user'],'*',[
           'AND'=>[
               's_email'=>$email,
               's_password'=>$password
           ]
        ]);
        /*
         * if user exist
         */
        if($user){
            /*
             * create token app access
             */
            $token=md5(date('Y-m-d H:i:s').$email);
            $token_data=array(
                'token'=>$token,
                'session_id'=>$user['session_id']
            );
            /*
             * insert to db
             */
            $db->insert($config['table']['token'],$token_data);

            $response['status']=true;
            $response['message']="login success";
            $response['data']=$user;
            $response['token']=$token;
        }else{
            $response['status']=false;
            $response['message']="invalid email/password";
        }
        break;
}
echo json_encode($response);