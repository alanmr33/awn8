<?php
/**
 * Created by PhpStorm.
 * User: alanmr
 * Date: 6/9/17
 * Time: 9:22 AM
 */
include 'config.php';
/*
 * validate token
 */
$postdata = json_decode(file_get_contents("php://input"));
$token=(isset($postdata->token))? $postdata->token : FALSE;

if($token){
    /*
    * check token
    */
    $tokenInfo=$db->get($config['table']['token'],'*',[
        'token'=>$token
    ]);
    /*
     * if token valid
     */
    if($tokenInfo){
        $prices_data=array();
        /*
         * harga weekend , weekday and holyday
         */
        $harga_1=$postdata->harga_1;
        $harga_2=$postdata->harga_2;
        $harga_3=$postdata->harga_3;

        $tgl_awal=date('Y-m-d',strtotime($postdata->date_start));
        $tgl_akhir=date('Y-m-d',strtotime($postdata->date_end));
        /*
         * check holyday
         */
        $holyday=$db->select($config['table']['libur'],'*',[
            'AND'=>[
                'dari[>=]'=>$tgl_awal,
                'hingga[>=]'=>$tgl_akhir
            ]
        ]);
        /*
         * check price
         */
        while (strtotime($tgl_awal)<=strtotime($tgl_akhir)){
            $holyday_is=false;
            foreach ($holyday as $h){
                if($tgl_awal>=$h['dari'] && $tgl_akhir<=$h['hingga']) {
                    $holyday_is = true;
                }
            }
            $N=date('N',strtotime($tgl_awal));
            /*
             * if holiday
             */
            if($holyday_is){
                $prices_data[]=array(
                    'night'=>date('l , d F Y',strtotime($tgl_awal)),
                    'price'=>$harga_3
                );
            }else{
                /*
                 * if friday-saturday
                 */
                if($N==5 || $N==6){
                    $prices_data[]=array(
                        'night'=>date('l , d F Y',strtotime($tgl_awal)),
                        'price'=>$harga_2
                    );
                }else{
                    $prices_data[]=array(
                        'night'=>date('l , d F Y',strtotime($tgl_awal)),
                        'price'=>$harga_1
                    );
                }
            }
            $tgl_awal=date('Y-m-d',strtotime($tgl_awal.' +1day'));
        }
        $response['status']=true;
        $response['message']="price received";
        $response['data']=$prices_data;
    }else{
        $response['status']=false;
        $response['message']="not authorized";
    }
}else{
    $response['status']=false;
    $response['message']="token needed";
}
echo json_encode($response);