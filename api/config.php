<?php
/**
 * Created by PhpStorm.
 * User: alanmr
 * Date: 6/9/17
 * Time: 9:22 AM
 */
include 'vendor/autoload.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Content-type:application/json");
header("Access-Control-Allow-Headers:Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
/*
 * configuration DB
 */
$config['db']['server']='localhost';
$config['db']['username']='goahead1_awn8';
$config['db']['password']='goahead1_awn8';
$config['db']['database_name']='goahead1_awn8';
$config['db']['database_type']='mysql';
$config['table']['user']="session";
$config['table']['token']="tokens";
$config['table']['info']="berita";
$config['table']['slide']="tujuan_slide";
$config['table']['tujuan']="tujuan";
$config['table']['penginapan']="penginapan";
$config['table']['pemesanan']="pemesanan";
$config['table']['libur']="libur_nasional";
$config['table']['bank']="bank";
$config['table']['polling']="poling";
$config['table']['pemesanan']="pemesanan";
$config['table']['detail_pemesanan']="detail_pemesanan";

/*
 * configuration Email
 */
$db=new \Medoo\Medoo($config['db']);
function registerMail($data){
    $subject="REGISTRASI MEMBER AGROWISATA N8";
    $content='
        <div>
                    <h3>Registrasi Member Agrowisata N8</h3>
                     <p>Terima kasih atas partisipasi anda, data member anda telah tersimpan dengan informasi sebagai berikut.</p>
                         <p></p>
                         <table cellspacing="0" width="500px">
                             <tbody>
                                 <tr>
                                    <td colspan="7" style="background-color:#85ab00;border-bottom:3px solid #ffb606;padding:10px;font-size:1.2em"><b>Info Member</b></td>
                                 </tr>
                                 <tr>
                                    <td style="background-color:#f3f3f3;font-weight:bolder;padding:5px">Nama</td><td style="background-color:#f3f3f3;padding:5px">:</td><td style="background-color:#f3f3f3;font-weight:bolder;padding:5px">'.$data['s_nama'].'</td>
                                 </tr>
                                 <tr>
                                    <td style="background-color:#f3f3f3;font-weight:bolder;padding:5px">Email</td><td style="background-color:#f3f3f3;padding:5px">:</td><td style="background-color:#f3f3f3;font-weight:bolder;padding:5px"><a href="mailto:[email]" target="_blank">'.$data['s_email'].'</a></td>
                                 </tr>
                                 <tr>
                                    <td style="background-color:#f3f3f3;font-weight:bolder;padding:5px">No Telepon</td><td style="background-color:#f3f3f3;padding:5px">:</td><td style="background-color:#f3f3f3;font-weight:bolder;padding:5px">'.$data['s_telp'].'</td>
                                 </tr>
                                 <tr>
                                    <td style="background-color:#f3f3f3;font-weight:bolder;padding:5px">Alamat</td><td style="background-color:#f3f3f3;padding:5px">:</td><td style="background-color:#f3f3f3;font-weight:bolder;padding:5px"><pre>'.$data['s_alamat'].'</pre></td>
                                 </tr>
                             </tbody>
                         </table>
                         <p></p>
                         <div style="background-color:#ebebeb;padding:10px;border-left:3px solid #ffb606">
                         <b style="font-size:16px;text-indent:25px">Ketentuan pemesanan</b>
                         <ul>
                          <li>Reservasi dianggap Sah apabila ada Konfirmasi dari Pihak Agrowisata N8</li>
                          <li>Dimohon Untuk Tidak Melakukan Down Payment (DP) / Uang Muka Sebelum Pihak Agrowisata Mengirimkan Faktur Harga ( Konfirmasi )</li>
                          <li>Apabila Belum ada Konfirmasi dalam jangka waktu <b>2 hari</b> setelah booking silahkan menghubungi kami di 022-2534517/022-2501635</li>
                          <li>Apabila tidak ada DP Dalam Jangka waktu tersebut, Maka Pihak Agrowisata N8 Berhak untuk melaksanakan Cancel sepihak</li>
                         </ul>
                         </div>
                         <p></p>
                         <p>Terima kasih</p>Salam kami,<p>Agrowisata N8
        
                    </p>
        </div>
    ';
    $contentalt='
         Registrasi Member Agrowisata N8 \n
         Terima kasih atas partisipasi anda, data member anda telah tersimpan dengan informasi sebagai berikut. \n
         nama   :  '.$data['s_nama'].' \n              
         email   :  '.$data['s_email'].' \n              
         no telepon   :  '.$data['s_telp'].' \n              
         alamat   :  '.$data['s_alamat'].' \n              
         Ketentuan pemesanan :  \n
         Reservasi dianggap Sah apabila ada Konfirmasi dari Pihak Agrowisata N8 \n
         Dimohon Untuk Tidak Melakukan Down Payment (DP) / Uang Muka Sebelum Pihak Agrowisata Mengirimkan Faktur Harga ( Konfirmasi ) \n
         Apabila Belum ada Konfirmasi dalam jangka waktu 2 hari setelah booking silahkan menghubungi kami di 022-2534517/022-2501635 \n
         Apabila tidak ada DP Dalam Jangka waktu tersebut, Maka Pihak Agrowisata N8 Berhak untuk melaksanakan Cancel sepihak \n
        \n
         Terima kasih Salam kami, \n
         \n
         \n
         \n
         Agrowisata N8
    ';
    sentMail($data['s_email'],$data['s_nama'],$subject,$content,$contentalt);
}
function sentMail($to,$toname,$sub,$isipesan,$altisi){
    $mail=new \PHPMailer\PHPMailer\PHPMailer();
    $mail->SMTPDebug = 0;                          // Enable verbose debug output
    $mail->isSMTP();                              // Set mailer to use SMTP
    $mail->Host = 'awn8.co.id';                  // Specify main and backup SMTP servers
    $mail->Port = 587;                          // TCP port to connect to
    $mail->SMTPAuth = true;
    $mail->SMTPsecure = 'tsl';
    $mail->Username = 'admin@awn8.co.id';        // SMTP username
    $mail->Password = 'adminawn8';                 // SMTP password
    $mail->From = 'admin@awn8.co.id';
    $mail->FromName = 'Agrowista N8';
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->SetFrom('admin@awn8.co.id', 'Agrowista N8');
    $mail->AddReplyTo('agrowisata_n8@yahoo.com','Agrowista N8');

    $mail->Subject = $sub;
    $mail->Body    = $isipesan;
    $mail->AltBody = $altisi;
    $mail->AddAddress($to, $toname);
    if(!$mail->send()){
        return false;
    }else{
        return true;
    }
}


