import {Component, Input} from '@angular/core';
import { NavController } from 'ionic-angular';
import {RegapiProvider} from '../../providers/regapi/regapi';
import {Storage} from '@ionic/storage';
/**
 * Generated class for the RoomsliderComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'roomslider',
  templateUrl: 'roomslider.html'
})
export class RoomsliderComponent {
  slides = [];
  currentIndex=0;
  @Input('kodeTujuan') kode_tujuan : any;
  constructor(public navCtrl: NavController,public regApi : RegapiProvider,public storage : Storage) {
    this.storage.get('user_token').then((val)=>{
      this.getSlides(val);
    });
  }
  getSlides(tokenData){
    this.regApi.getDestinationSlide(this.kode_tujuan,tokenData).subscribe(data=>this.bindData(data));
  }
  bindData(data){
    this.slides=data.data;
  }
}
