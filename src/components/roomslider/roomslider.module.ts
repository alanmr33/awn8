import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RoomsliderComponent } from './roomslider';

@NgModule({
  declarations: [
    RoomsliderComponent,
  ],
  imports: [
    IonicPageModule.forChild(RoomsliderComponent),
  ],
  exports: [
    RoomsliderComponent
  ]
})
export class RoomsliderComponentModule {}
