import { Component } from '@angular/core';
import {NavController} from "ionic-angular";
import {Storage} from '@ionic/storage';
import {HomePage} from '../../pages/home/home';
import {InfopromoPage} from '../../pages/infopromo/infopromo';
import {UserinfoPage} from '../../pages/userinfo/userinfo';
import {BookinglistPage} from '../../pages/bookinglist/bookinglist';
import {SelectdestinationPage} from '../../pages/selectdestination/selectdestination';
/**
 * Generated class for the AppmenuComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'appmenu',
  templateUrl: 'appmenu.html'
})
export class AppmenuComponent {
  email="";
  infoPage=InfopromoPage;
  userPage=UserinfoPage;
  desPage=SelectdestinationPage;
  blPage=BookinglistPage;
  constructor(public navCtrl: NavController, public storage : Storage) {
    this.storage.get('user_email').then((val)=>{
      this.email=val;
    });
  }
  logOut(){
    this.storage.set('user_login','false');
    this.storage.set('user_token','');
    this.storage.set('user_cart','');
    this.navCtrl.setRoot(HomePage);
  }
  backHome(){
    this.navCtrl.popToRoot();
  }
}
