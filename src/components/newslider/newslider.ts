import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {RegapiProvider} from '../../providers/regapi/regapi';
import {Storage} from '@ionic/storage';
import {DetailinfoPage} from '../../pages/detailinfo/detailinfo';
/**
 * Generated class for the NewsliderComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'newslider',
  templateUrl: 'newslider.html'
})
export class NewsliderComponent {
  slides = [];
  currentIndex=0;
  params={};
  detailPage=DetailinfoPage;
  constructor(public navCtrl: NavController,public regApi : RegapiProvider,public storage : Storage) {
    this.storage.get('user_token').then((val)=>{
      this.getSlides(val);
    });
  }
  getSlides(tokenData){
      this.regApi.getSlides(tokenData).subscribe(data=>this.bindData(data));
  }
  bindData(data){
    this.slides=data.data;
    if(this.slides.length>0){
      this.params=this.slides[0];
    }
    this.changeIndex();
  }
  changeIndex(){
    var tmp=this;
    setInterval(function () {
      if(tmp.currentIndex!=(tmp.slides.length-1)){
        tmp.currentIndex++;
        var newIndex=tmp.currentIndex;
        tmp.params=tmp.slides[newIndex];
      }else{
        tmp.currentIndex=0;
        if(tmp.slides.length>0){
          tmp.params=tmp.slides[0];
        }
      }
    },6000);
  }
}
