import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RsvformPage } from './rsvform';

@NgModule({
  declarations: [
    RsvformPage,
  ],
  imports: [
    IonicPageModule.forChild(RsvformPage),
  ],
  exports: [
    RsvformPage
  ]
})
export class RsvformPageModule {}
