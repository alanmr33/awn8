import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {RegapiProvider} from "../../providers/regapi/regapi";
/**
 * Generated class for the RsvformPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-rsvform',
  templateUrl: 'rsvform.html',
})
export class RsvformPage {
  bank=[];
  polling=[];
  selectedBank="";
  selectedpolling="";
  note="";
  cartData=[];
  grandTotal=0;
  tokenData="";
  constructor(public navCtrl: NavController, public navParams: NavParams,public storage : Storage, public regApi : RegapiProvider, private alertCtrl : AlertController) {
    this.storage.get('user_token').then((val)=>{
      this.getBank(val);
      this.getPolling(val);
      this.tokenData=val;
    });
    this.storage.get('user_cart').then((val)=> {
      if (val) {
        this.cartData=JSON.parse(val);
        for(let i=0;i<this.cartData.length;i++){
          this.grandTotal+=this.cartData[i].grandTotal;
        }
      }
    });
  }
  getBank(tokenData){
    this.regApi.getBanks(tokenData).subscribe((data)=>this.bank=data.data);
  }
  getPolling(tokenData){
    this.regApi.getPolling(tokenData).subscribe((data)=>this.polling=data.data);
  }
  setPol(val){
    this.selectedpolling=val;
  }
  setBank(val){
    this.selectedBank=val;
  }
  processReservation(){
  this.regApi.checkOut(this.selectedBank,this.selectedpolling,this.note,this.cartData,this.tokenData).subscribe((data)=>this.handleResponse(data));
  }
  handleResponse(data){
    if(data.status){
      let alert = this.alertCtrl.create({
        title: 'Agrowisata N8',
        message: data.message,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              this.storage.set('user_cart','');
              this.navCtrl.popToRoot();
            }
          }]
      });
      alert.present();
    }else{
      let alert = this.alertCtrl.create({
        title: 'Agrowisata N8',
        message: data.message,
        buttons: ['Dismiss']
      });
      alert.present();
    }
  }
}
