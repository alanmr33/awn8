import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import {RegapiProvider} from "../../providers/regapi/regapi";
import {DestinationPage} from "../../pages/destination/destination";
import {Storage} from '@ionic/storage';
import {CheckbookingPage} from "../checkbooking/checkbooking";
import {CheckoutPage} from "../checkout/checkout";

/**
 * Generated class for the SelectdestinationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-selectdestination',
  templateUrl: 'selectdestination.html',
})
export class SelectdestinationPage {
  destination = [];
  desPage=DestinationPage;
  checkPage=CheckbookingPage;
  coPage=CheckoutPage;
  constructor(public navCtrl: NavController,public regApi : RegapiProvider,public storage : Storage ) {
    this.storage.get('user_token').then((val)=>{
      this.getDestination(val);
    });
  }
  getDestination(tokenData){
    this.regApi.getDestination(tokenData).subscribe(data=>this.destination=data.data);
  }
}
