import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectdestinationPage } from './selectdestination';

@NgModule({
  declarations: [
    SelectdestinationPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectdestinationPage),
  ],
  exports: [
    SelectdestinationPage
  ]
})
export class SelectdestinationPageModule {}
