import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {SelectdestinationPage} from "../selectdestination/selectdestination";
import {RsvformPage} from "../rsvform/rsvform";

/**
 * Generated class for the CheckoutPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {
  cartData=[];
  grandTotal=0;
  destPage=SelectdestinationPage;
  formPage=RsvformPage;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage : Storage) {
    this.storage.get('user_cart').then((val)=> {
      if (val) {
        this.cartData=JSON.parse(val);
        for(let i=0;i<this.cartData.length;i++){
          this.grandTotal+=this.cartData[i].grandTotal;
        }
      }
    });
  }
  deleteItem(index){
    this.grandTotal=0;
    this.cartData.splice(index,1);
    this.storage.set('user_cart',JSON.stringify(this.cartData));
    for(let i=0;i<this.cartData.length;i++){
      this.grandTotal+=this.cartData[i].grandTotal;
    }
  }
}
