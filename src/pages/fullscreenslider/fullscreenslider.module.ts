import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FullscreensliderPage } from './fullscreenslider';

@NgModule({
  declarations: [
    FullscreensliderPage,
  ],
  imports: [
    IonicPageModule.forChild(FullscreensliderPage),
  ],
  exports: [
    FullscreensliderPage
  ]
})
export class FullscreensliderPageModule {}
