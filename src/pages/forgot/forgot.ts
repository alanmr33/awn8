/**
 * Created by alanmr on 6/10/17.
 */
import { Component } from '@angular/core';
import {NavController,AlertController } from 'ionic-angular';
import {RegapiProvider} from '../../providers/regapi/regapi';
import {IonicPage} from "ionic-angular";
@IonicPage()
@Component({
  selector: 'page-forgot',
  templateUrl: 'forgot.html',
})
export class ForgotPage {
  emailForgot = "";
  messageError="";
  constructor(public navCtrl: NavController, public regApi : RegapiProvider, public alertCtrl : AlertController) {

  }
  sendForgot(){
    this.regApi.userForgot(this.emailForgot).subscribe(data=>this.forgotHandler(data));
  }
  /*
  handle request
   */
  forgotHandler(data){
    if(data.status){
        let alert=this.alertCtrl.create({
            title : 'Reset Password',
            subTitle : data.message,
            buttons : ['OK']
        });
        alert.present();
    }else{
      this.messageError=data.message;
    }
  }
}
