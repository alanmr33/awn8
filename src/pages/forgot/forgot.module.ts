/**
 * Created by alanmr on 6/11/17.
 */
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ForgotPage } from './forgot';

@NgModule({
  declarations: [
    ForgotPage,
  ],
  imports: [
    IonicPageModule.forChild(ForgotPage)
  ],
  exports: [
    ForgotPage
  ]
})
export class ForgotPageModule {}
