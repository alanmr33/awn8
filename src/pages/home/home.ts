import { Component } from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import { RegisterPage } from '../register/register';
import {ForgotPage} from '../forgot/forgot';
import {MainPage} from '../main/main';
import {RegapiProvider} from '../../providers/regapi/regapi';
import {Storage} from '@ionic/storage';
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  loginData = {
    email : '',
    password : ''
  };
  messageError="";
  forgotPage=ForgotPage;
  constructor(public navCtrl: NavController, public regApi: RegapiProvider, public storage : Storage) {
    this.storage.get('user_login').then((val)=>{
      if(val=='true'){
        this.navCtrl.setRoot(MainPage);
      }
    });
  }
  goRegister(){
    this.navCtrl.push(RegisterPage);
  }
  sendLogin(){
    this.regApi.userLogin(this.loginData).subscribe(data => this.loginHandler(data));
  }
  /*
  proceed response from server
   */
  loginHandler(data){
    if(data.status){
      var userdata=data.data;
      this.storage.set('user_login','true');
      this.storage.set('user_fullname',userdata.s_nama);
      this.storage.set('user_email',userdata.s_email);
      this.storage.set('user_nohp',userdata.s_telp);
      this.storage.set('user_alamat',userdata.s_alamat);
      this.storage.set('user_token',data.token);
      this.navCtrl.setRoot(MainPage);
    }else{
      this.messageError=data.message;
    }
  }
}
