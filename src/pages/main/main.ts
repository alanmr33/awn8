import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {RegapiProvider} from "../../providers/regapi/regapi";
import {DestinationPage} from "../destination/destination";
/**
 * Generated class for the MainPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {
  destination = [];
  desPage=DestinationPage;
  constructor(public navCtrl: NavController,public regApi : RegapiProvider, public storage : Storage) {
    this.storage.get('user_token').then((val)=>{
      this.getDestination(val);
    });
  }
  getDestination(tokenData){
    this.regApi.getDestination(tokenData).subscribe(data=>this.destination=data.data);
  }
}
