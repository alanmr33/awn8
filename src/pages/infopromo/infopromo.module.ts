import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfopromoPage } from './infopromo';

@NgModule({
  declarations: [
    InfopromoPage,
  ],
  imports: [
    IonicPageModule.forChild(InfopromoPage),
  ],
  exports: [
    InfopromoPage
  ]
})
export class InfopromoPageModule {}
