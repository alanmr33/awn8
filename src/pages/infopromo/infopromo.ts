import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import {RegapiProvider} from '../../providers/regapi/regapi';
import {Storage} from '@ionic/storage';
import {DetailinfoPage} from '../../pages/detailinfo/detailinfo';
/**
 * Generated class for the InfopromoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-infopromo',
  templateUrl: 'infopromo.html',
})
export class InfopromoPage {
  promos=[];
  detailPage=DetailinfoPage;
  constructor(public navCtrl: NavController, public regApi: RegapiProvider, public storage : Storage) {
    this.storage.get('user_token').then((val)=>{
      this.getPromo(val);
    });
  }
  getPromo(tokenData){
    this.regApi.getSlides(tokenData).subscribe(data=>this.promos=data.data);
  }
}
