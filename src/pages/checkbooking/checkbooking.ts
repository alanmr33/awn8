import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {RegapiProvider} from "../../providers/regapi/regapi";
import {BookroomPage} from "../bookroom/bookroom";
import {Storage} from '@ionic/storage';
/**
 * Generated class for the CheckbookingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-checkbooking',
  templateUrl: 'checkbooking.html',
})
export class CheckbookingPage {
  dataDate={
    date_start: this.getCurrentDate(),
    date_end: null,
    maxYear : new Date().getFullYear()+2,
    date_now: this.getCurrentDate(),
  };
  rooms=[];
  title="";
  kode_tujuan="";
  tokenData='';
  bookingPage=BookroomPage;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage : Storage, public regApi : RegapiProvider) {
    this.title=this.navParams.get('nama_tempat');
    this.kode_tujuan=this.navParams.get('kode_tujuan');
    this.storage.get('user_token').then((val)=>{
     this.tokenData=val;
    });
  }
  getCurrentDate(){
    var date=new Date();
    var year=date.getFullYear();
    var month=((date.getMonth()+1)<10)? '0'+date.getMonth() : date.getMonth();
    var day=((date.getDay()+1)<10)? '0'+date.getDay() : date.getDay();
    return year+'-'+month+'-'+day;
  }
  checkRooms(){
    this.regApi.checkRooms(this.kode_tujuan,this.dataDate.date_start,this.dataDate.date_end,this.tokenData).subscribe(data=>this.rooms=data.data);
  }
}
