import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckbookingPage } from './checkbooking';

@NgModule({
  declarations: [
    CheckbookingPage,
  ],
  imports: [
    IonicPageModule.forChild(CheckbookingPage),
  ],
  exports: [
    CheckbookingPage
  ]
})
export class CheckbookingPageModule {}
