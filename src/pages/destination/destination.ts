import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CheckbookingPage} from '../checkbooking/checkbooking';
/**
 * Generated class for the DestinationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-destination',
  templateUrl: 'destination.html',
})
export class DestinationPage {
  title="";
  images="";
  description="";
  map="";
  lokasi="";
  kode_tujuan="";
  checkPage=CheckbookingPage;
  params={};
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.title=this.navParams.get("nama_tempat");
    this.images=this.navParams.get("gambar_utama");
    this.description=this.navParams.get("desk_indo");
    this.map=this.navParams.get("linkmap");
    this.lokasi=this.navParams.get("alamat");
    this.kode_tujuan=this.navParams.get("kode_tujuan");
    this.params=this.navParams;
  }
}
