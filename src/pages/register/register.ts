import { Component } from '@angular/core';
import { IonicPage,NavController } from 'ionic-angular';
import {RegapiProvider} from '../../providers/regapi/regapi';
import {Storage} from '@ionic/storage';
import {MainPage} from '../main/main';
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {
  registerData = {
    fullname : '',
    email : '',
    password : '',
    cpassword : '',
    nohp : '',
    address : ''
  };
  messageError="";
  constructor(public navCtrl: NavController,public regApi : RegapiProvider,public storage : Storage) {

  }
  sendRegister(){
    this.regApi.userRegister(this.registerData).subscribe(data => this.registerHandler(data));
  }
  /*
  handle Register
   */
  registerHandler(data){
    if(data.status){
      this.storage.set('user_login','true');
      this.storage.set('user_fullname',this.registerData.fullname);
      this.storage.set('user_email',this.registerData.email);
      this.storage.set('user_nohp',this.registerData.nohp);
      this.storage.set('user_alamat',this.registerData.address);
      this.storage.set('user_token',data.token);
      this.navCtrl.setRoot(MainPage);
    }else{
      this.messageError=data.message;
    }
  }
}
