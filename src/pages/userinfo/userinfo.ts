import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import {RegapiProvider} from '../../providers/regapi/regapi';
import {Storage} from '@ionic/storage';
/**
 * Generated class for the UserinfoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-userinfo',
  templateUrl: 'userinfo.html',
})
export class UserinfoPage {
  userData = {
    fullname : '',
    email : '',
    password : '',
    cpassword : '',
    opassword : '',
    nohp : '',
    address : ''
  };
  constructor(public navCtrl: NavController,public regApi : RegapiProvider,public storage : Storage) {
    this.getUserInfo();
  }
  getUserInfo(){
    this.storage.get('user_fullname').then((val)=>{this.userData.fullname=val});
    this.storage.get('user_email').then((val)=>{this.userData.email=val});
    this.storage.get('user_alamat').then((val)=>{this.userData.address=val});
    this.storage.get('user_nohp').then((val)=>{this.userData.nohp=val});
  }
  updateUserData(){

  }
}
