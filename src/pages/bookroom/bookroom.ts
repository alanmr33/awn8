import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {RegapiProvider} from "../../providers/regapi/regapi";
import {CheckoutPage} from '../../pages/checkout/checkout';
import {Storage} from '@ionic/storage';

/**
 * Generated class for the BookroomPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-bookroom',
  templateUrl: 'bookroom.html',
})
export class BookroomPage {
  place='';
  room={
    harga_1 : 0,
    harga_2 : 0,
    harga_3 : 0
  };
  prices=[];
  dateData={
    date_start : '',
    date_end : ''
  };
  grandTotal=0;
  cart=[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage : Storage, public regApi : RegapiProvider) {
    this.place=this.navParams.get('title');
    this.room=this.navParams.get('room');
    this.dateData=this.navParams.get('dateData');
    this.storage.get('user_token').then((val)=>{
      this.getDataPrices(val);
    });
  }
  getDataPrices(tokenData){
    this.regApi.checkPrices(this.room.harga_1,this.room.harga_2,this.room.harga_3,this.dateData.date_start,this.dateData.date_end,tokenData).subscribe(data=>this.countTotal(data.data));
  }
  countTotal(data){
    this.grandTotal=0;
    this.prices=data;
    for(let i=0;i<this.prices.length;i++){
      this.grandTotal+=Number(this.prices[i].price);
    }
  }
  addToCart(){
    this.storage.get('user_cart').then((val)=>{
      if(val){
        this.cart=JSON.parse(val);
      }
      this.cart.push({room : this.room ,tujuan : this.place, dateData: this.dateData, priceData : this.prices , grandTotal : this.grandTotal});
      this.storage.set('user_cart',JSON.stringify(this.cart));
      this.navCtrl.push(CheckoutPage);
    });
  }
}
