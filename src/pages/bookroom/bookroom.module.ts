import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookroomPage } from './bookroom';

@NgModule({
  declarations: [
    BookroomPage,
  ],
  imports: [
    IonicPageModule.forChild(BookroomPage),
  ],
  exports: [
    BookroomPage
  ]
})
export class BookroomPageModule {}
