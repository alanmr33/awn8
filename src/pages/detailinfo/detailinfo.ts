import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DetailinfoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-detailinfo',
  templateUrl: 'detailinfo.html',
})
export class DetailinfoPage {
  title='';
  images='';
  content='';
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.title=this.navParams.get('berita_judul');
    this.images=this.navParams.get('berita_gambar');
    this.content=this.navParams.get('berita_isi');
  }

}
