import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/Observable';
import {Observable} from "rxjs/Observable";

/*
  Generated class for the RegapiProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class RegapiProvider {
  private site_url : string="http://awn8.co.id/api/";
  private login_api : string=this.site_url+'login.php';
  private register_api : string=this.site_url+'register.php';
  private forgot_api : string=this.site_url+'forgot.php';
  private update_api : string=this.site_url+'update_user.php';
  private news_api : string=this.site_url+'getinfo.php';
  private destination_api : string=this.site_url+'gettujuan.php';
  private slide_api : string=this.site_url+'getslides.php';
  private check_api : string=this.site_url+'check_availability.php';
  private check_price_api : string=this.site_url+'checkharga.php';
  private bank_api : string=this.site_url+'getbank.php';
  private polling_api : string=this.site_url+'getpolling.php';
  private checkout_api : string=this.site_url+'checkout.php';
  constructor(private http: Http) {
  }
  /*
  send login data
   */
  userLogin(loginData){
    return this.http.post(this.login_api,loginData).do(this.logNetwork).map(this.extractData).catch(this.errorHandling);
  }
  /*
  send register data
   */
  userRegister(registerData){
    return this.http.post(this.register_api,registerData).do(this.logNetwork).map(this.extractData).catch(this.errorHandling);
  }
  /*
 send forgot request
 */
  userForgot(emailData){
    return this.http.post(this.forgot_api,{email:emailData}).do(this.logNetwork).map(this.extractData).catch(this.errorHandling);
  }
  /*
   get News
   */
  getSlides(tokenData){
    return this.http.post(this.news_api,{token:tokenData}).do(this.logNetwork).map(this.extractData).catch(this.errorHandling);
  }
  /*
   get Bank
   */
  getBanks(tokenData){
    return this.http.post(this.bank_api,{token:tokenData}).do(this.logNetwork).map(this.extractData).catch(this.errorHandling);
  }
  /*
   get Polling
   */
  getPolling(tokenData){
    return this.http.post(this.polling_api,{token:tokenData}).do(this.logNetwork).map(this.extractData).catch(this.errorHandling);
  }
  /*
   get Destination
   */
  getDestination(tokenData){
    return this.http.post(this.destination_api,{token:tokenData}).do(this.logNetwork).map(this.extractData).catch(this.errorHandling);
  }
  /*
   get Destination Slide
   */
  getDestinationSlide(kodeTujuan,tokenData){
    return this.http.post(this.slide_api,{kode_tujuan: kodeTujuan,token:tokenData}).do(this.logNetwork).map(this.extractData).catch(this.errorHandling);
  }
  /*
   check Available Room
   */
  checkRooms(kodeTujuan,dateStart,dateEnd,tokenData){
    return this.http.post(this.check_api,{kode_tujuan: kodeTujuan, date_start : dateStart, date_end : dateEnd,token:tokenData}).do(this.logNetwork).map(this.extractData).catch(this.errorHandling);
  }
  /*
  check prices
   */
  checkPrices(harga1,harga2,harga3, date_start,date_end,tokenData){
    return this.http.post(this.check_price_api,{harga_1 : harga1,harga_2 : harga2,harga_3 : harga3,date_start : date_start, date_end : date_end, token:tokenData}).do(this.logNetwork).map(this.extractData).catch(this.errorHandling);
  }
  checkOut(bank,ref,note,cart,tokenData){
    return this.http.post(this.checkout_api,{bank: bank,note: note,ref : ref,detail : cart,token:tokenData}).do(this.logNetwork).map(this.extractData).catch(this.errorHandling);
  }
  /*
  loging API
   */
  private logNetwork(res: Response | any){
    console.log(res);
  }
  /*
  map json response
   */
  private extractData(res : Response | any){
    return res.json();
  }
  /*
  error handling
   */
  private errorHandling(error : Response | any){
    console.log(error);
    return Observable.throw(error || "server error");
  }
}
