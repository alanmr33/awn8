import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { RegisterPage } from '../pages/register/register';
import { ForgotPage } from '../pages/forgot/forgot';
import { DetailinfoPage } from '../pages/detailinfo/detailinfo';
import { InfopromoPage } from '../pages/infopromo/infopromo';
import { CheckoutPage } from '../pages/checkout/checkout';
import { DestinationPage } from '../pages/destination/destination';
import { FullscreensliderPage } from '../pages/fullscreenslider/fullscreenslider';
import { UserinfoPage } from '../pages/userinfo/userinfo';
import { BookroomPage } from '../pages/bookroom/bookroom';
import { BookinglistPage } from '../pages/bookinglist/bookinglist';
import { CheckbookingPage } from '../pages/checkbooking/checkbooking';
import { RsvformPage } from '../pages/rsvform/rsvform';
import { SelectdestinationPage } from '../pages/selectdestination/selectdestination';
import { RegapiProvider } from '../providers/regapi/regapi';
import { AppmenuComponent } from '../components/appmenu/appmenu';
import {MainPage} from "../pages/main/main";
import { NewsliderComponent } from '../components/newslider/newslider';
import { RoomsliderComponent } from '../components/roomslider/roomslider';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    RegisterPage,
    ForgotPage,
    AppmenuComponent,
    MainPage,
    NewsliderComponent,
    DetailinfoPage,
    InfopromoPage,
    UserinfoPage,
    SelectdestinationPage,
    DestinationPage,
    RoomsliderComponent,
    FullscreensliderPage,
    CheckbookingPage,
    BookroomPage,
    CheckoutPage,
    BookinglistPage,
    RsvformPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    IonicStorageModule.forRoot({
      name: 'app_awn8',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    RegisterPage,
    ForgotPage,
    MainPage,
    DetailinfoPage,
    InfopromoPage,
    UserinfoPage,
    SelectdestinationPage,
    DestinationPage,
    FullscreensliderPage,
    CheckbookingPage,
    BookroomPage,
    CheckoutPage,
    BookinglistPage,
    RsvformPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RegapiProvider
  ]
})
export class AppModule {}
